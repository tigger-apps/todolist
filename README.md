# TodoList

## Screens

* Overview: all lists
  in headline: [+] new list, [i] info screen
  on entry: [onClick] show list, [e] editList, [^] up, [v] down

* Show list
  in headline: [<-] back to overview, [+] new entry, [e] edit list, [d] delete list
  on entry: [^] up, [v] down, [e] edit entry, [d] delete entry

* Edit entry
  in headline: [<-] back to list, [d] delete]

* edit list
  name
  color


## Icons

* plus     +
* edit     +
* delete   +
* up       +
* down     +
* info     +
* back     +
* appIcon  [SVG repo](https://www.svgrepo.com/svg/65100/list)
